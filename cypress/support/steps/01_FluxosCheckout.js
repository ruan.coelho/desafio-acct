
/* global Given, Then, When */

import FluxosCheckout from "../pageobjects/01_FluxosCheckoutPage"

const fluxosCheckout = new FluxosCheckout

Given("eu acesse o site da Polishop e esteja em uma pdp", () => {
    fluxosCheckout.buscarProduto();
    fluxosCheckout.abrirPdpPrimeiroProduto();
})

When("eu insiro um produto no carrinho", () => {
    fluxosCheckout.comprarProduto();
})

And("eu clique em fechar pedido", () => {
    fluxosCheckout.fecharPedidoPdp();
})

And("eu preencha as informações necessárias", () => {
    fluxosCheckout.preencherInformacoesCheckout();

})

Then("eu devo ser levado para tela de pagamentos", () => {
    fluxosCheckout.clicarIrParaPagamento();
    fluxosCheckout.verificaPaginaPagamento();
})

And("eu devo escolher a opção boleto", () => {
    fluxosCheckout.escolherBoleto();
})

When("eu clico em Ver Frete e Opções de Entrega", () => {
    fluxosCheckout.clicarVerFrete();
})

And("eu insiro um CEP e busco", () => {
    fluxosCheckout.digitarCep();
})

Then("o valor e prazo de entrega serão exibidos na tela", ()=> {
    fluxosCheckout.verificarEntrega();
})

And("eu insiro outro produto no carrinho", () => {
    fluxosCheckout.buscarProduto();
    fluxosCheckout.abrirPdpSegundoProduto();
    fluxosCheckout.comprarProduto();
})

And("eu acesse o carrinho e clique para limpá-lo", () => {
    fluxosCheckout.removerItensCarrinho();
})

Then("o carrinho deve estar limpo", () => {
    fluxosCheckout.verificaCarrinhoVazio();
})
