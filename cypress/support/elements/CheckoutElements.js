class CheckoutElements{

    btnFecharPedido = () => { return '#cart-to-orderform'}

    btnEscolherMaisProdutos = () => { return '#cart-choose-more-products'}

    campoEmail = () => { return '#client-pre-email'}

    btnContinuar = () => { return '#btn-client-pre-email'}

    campoNome = () => { return '#client-first-name'}

    campoSobrenome = () => { return '#client-last-name'}

    campoCpf = () => { return '#client-document'}

    campoTelefone = () => { return '#client-phone'}

    btnIrParaEntrega = () => { return '#go-to-shipping'}

    btnVerFrete = () => { return '.polishop-io-shipping-simulator-0-x-ShippingSimulationTriggerBtn'}

    campoCepCheckout = () => { return '#ship-postalCode'}
    
    campoNumeroCheckout = () => { return '#ship-number'}

    btnIrParaPagamento = () => { return'#btn-go-to-payment'}

    btnBoletoBancarario = () => { return'#payment-group-bankInvoicePaymentGroup > .payment-group-item-text'}

    btnCartaoCredito = () => { return '#payment-group-creditCardPaymentGroup'}

    btnMercadoPago = () => { return '#payment-group-MercadoPagoPaymentGroup'}

    btnPicPay = () => { return '#payment-group-MercadoPagoPaymentGroup'}

    btnPix = () => { return '#payment-group-instantPaymentPaymentGroup'}

    btnCartaoPolishop = () => { return '#payment-group-customPrivate_401PaymentGroup'}

    btnProvuParcelado = () => { return '#payment-group-LendicoPaymentGroup'}

    btnCartaoDebito = () => { return '#payment-group-debitCardPaymentGroup' }

    btnRemoveItem = () => {return '.item-link-remove'}

    campoItem = () => { return '.product-item'}

    campoDescricaoPagamento = () => { return '.payment-description'}

    campoCarrinhoVazinho = () => { return '.empty-cart-title'}
}
export default CheckoutElements;