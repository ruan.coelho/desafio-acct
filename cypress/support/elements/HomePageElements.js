class HomePageElements {

    campoBusca = () => { return '#downshift-0-input' }

    btnBusca = () => { return '.polishop-io-header-0-x-autoCompleteOuterContainer > .w-100.flex > .vtex-input > .vtex-input-prefix__group > .c-muted-2' }

    primeiroProduto = () => { return ':nth-child(1) > .vtex-product-summary-2-x-container > .vtex-product-summary-2-x-clearLink > .vtex-product-summary-2-x-element > .vtex-stack-layout-0-x-stackContainer > [style="z-index: 6;"] > .vtex-product-summary-2-x-imageWrapper > .vtex-store-components-3-x-discountContainer > .dib > .vtex-product-summary-2-x-imageNormal' }

    segundoProduto = () => { return ':nth-child(2) > .vtex-product-summary-2-x-container > .vtex-product-summary-2-x-clearLink > .vtex-product-summary-2-x-element > .vtex-stack-layout-0-x-stackContainer > [style="z-index: 6;"] > .vtex-product-summary-2-x-imageWrapper > .vtex-store-components-3-x-discountContainer > .dib > .vtex-product-summary-2-x-imageNormal' }  
}
export default HomePageElements;