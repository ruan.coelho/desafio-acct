class PdpElements {

    btnComprar = () => { return '.vtex-flex-layout-0-x-flexColChild > .vtex-button' }

    btnVerFrete = () => { return '.polishop-io-shipping-simulator-0-x-ShippingSimulationTriggerBtn' }

    campoCep = () => { return '.polishop-io-shipping-simulator-0-x-input' }

    btnBuscarCep = () => { return '.polishop-io-shipping-simulator-0-x-searchBtn' }

    titleOpcoesEntrega = () => { return ".polishop-io-shipping-simulator-0-x-optionsBlockTitle" }

    btnFecharModalCep = () => { return "vtex-modal__close-icon" }
}
export default PdpElements;