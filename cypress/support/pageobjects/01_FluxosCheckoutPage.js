
import CheckoutElements from "../elements/CheckoutElements"
import HomePageElements from "../elements/HomePageElements"
import PdpElements from "../elements/PdpElements"

const homePageElements = new HomePageElements
const pdpElements = new PdpElements
const checkoutElements = new CheckoutElements
const url = Cypress.config("baseUrl")
const faker = require('faker-br')

class FluxosCheckout {

    acessarSite() {
        cy.visit(url)
    }

    buscarProduto() {
        cy.wait(2000)
        this.acessarSite()
        cy.wait(1000)
        cy.get(homePageElements.campoBusca())
            .type("Frigideira")
            .type("{enter}")
    }

    abrirPdpPrimeiroProduto() {
        cy.get(homePageElements.primeiroProduto())
            .first()
            .should('be.visible')
            .click()
    }

    abrirCarrinho() {
        cy.get(homePageElements.btnCarrinho())
            .click()
    }

    abrirPdpSegundoProduto() {
        cy.get(homePageElements.segundoProduto())
            .first()
            .should('be.visible')
            .click()
    }

    comprarProduto() {
        cy.get(pdpElements.btnComprar())
            .click()
    }

    digitarCep() {
        cy.get(pdpElements.btnVerFrete())
            .click({ force: true })

        cy.get(pdpElements.campoCep())
            .click({ force: true })
            .type(faker.address.zipCodeValid(), { force: true })


        cy.get(pdpElements.btnBuscarCep())
            .click({ force: true })
    }

    verificarEntrega() {
        cy.get(pdpElements.titleOpcoesEntrega())
            .should('be.visible')

    }

    fecharPedidoPdp() {
        cy.get(checkoutElements.btnFecharPedido())
            .should('be.visible')
            .click()
    }

    digitarEmail() {
        cy.get(checkoutElements.campoEmail())
            .type("desafioacct@gmail.com")

        cy.get(checkoutElements.btnContinuar())
            .click()
    }

    digitarNome() {
        cy.get(checkoutElements.campoNome())
            .type(faker.name.firstName())
    }

    digitarSobrenome() {
        cy.get(checkoutElements.campoSobrenome())
            .type(faker.name.lastName())
    }

    digitarCepCheckout() {
        cy.get(checkoutElements.campoCepCheckout())
            .type(faker.address.zipCodeValid())
    }

    digitarTelefone() {
        cy.get(checkoutElements.campoTelefone())
            .type("11988778996")
    }

    digitarCpf() {
        cy.get(checkoutElements.campoCpf())
            .type(faker.br.cpf())
    }

    clicarIrParaEntrega() {
        cy.get(checkoutElements.btnIrParaEntrega())
            .should('be.visible')
            .click()
    }

    clicarIrParaPagamento() {
        cy.get(checkoutElements.btnIrParaPagamento())
            .should('be.visible')
            .click()
    }

    clicarVerFrete() {
        cy.get(checkoutElements.btnVerFrete())
            .should('be.visible')
            .click()
    }

    digitarNumero() {
        cy.get(checkoutElements.campoNumeroCheckout())
            .type(faker.random.number(1000))
    }

    preencherInformacoesCheckout() {
        this.digitarEmail()
        this.digitarNome()
        this.digitarSobrenome()
        this.digitarCpf()
        this.digitarTelefone()
        this.clicarIrParaEntrega()
        this.digitarCepCheckout()
        this.digitarNumero()
    }

    verificaPaginaPagamento() {
        cy.get(checkoutElements.btnCartaoCredito())
            .should('be.visible')

        cy.get(checkoutElements.btnMercadoPago())
            .should('be.visible')

        cy.get(checkoutElements.btnPicPay())
            .should('be.visible')

        cy.get(checkoutElements.btnBoletoBancarario())
            .should('be.visible')

        cy.get(checkoutElements.btnPix())
            .should('be.visible')

        cy.get(checkoutElements.btnCartaoPolishop())
            .should('be.visible')

        cy.get(checkoutElements.btnCartaoDebito())
            .should('be.visible')

        cy.get(checkoutElements.btnProvuParcelado())
            .should('be.visible')
    }

    escolherBoleto() {
        cy.get(checkoutElements.btnBoletoBancarario())
            .should('be.visible')
            .click()
    }

    abrirCarrinho() {
        cy.get(homePageElements.btnCarrinho())
            .click()
    }

    removerItensCarrinho() {
        cy.get(checkoutElements.btnRemoveItem())
            .first()
            .click()
            .reload()

        cy.get(checkoutElements.btnRemoveItem())
            .click({ force: true })
    }

    verificaCarrinhoVazio() {
        cy.get(checkoutElements.campoCarrinhoVazinho())
            .should('be.visible')
            .should('contain','Seu carrinho está vazio.')
    }
}
export default FluxosCheckout;