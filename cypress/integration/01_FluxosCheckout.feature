Feature: Fluxos de Compras

    Scenario: fluxo completo de compra
        Given eu acesse o site da Polishop e esteja em uma pdp
        When eu insiro um produto no carrinho
        And eu clique em fechar pedido
        And eu preencha as informações necessárias
        Then eu devo ser levado para tela de pagamentos
        And eu devo escolher a opção boleto
    
    Scenario: realizar calculo de frete na PDP
        Given eu acesse o site da Polishop e esteja em uma pdp
        When eu clico em Ver Frete e Opções de Entrega
        And eu insiro um CEP e busco
        Then o valor e prazo de entrega serão exibidos na tela

    Scenario: Inserindo 2 produtos e limpando o carrinho
        Given eu acesse o site da Polishop e esteja em uma pdp
        When eu insiro um produto no carrinho
        And eu insiro outro produto no carrinho
        And eu acesse o carrinho e clique para limpá-lo 
        Then o carrinho deve estar limpo 
        